/*Autotrace Enabled
Shows the execution plan as well as statistics of the statement.*/


--overeni, ze exisuji objednavky bez uzaverky ktere mohou patrit k nove uzaverce
SELECT COUNT(orderid) FROM guest_order WHERE closureid is NULL and close_date <= TO_DATE('2015.12.31 15:30.00','YYYY.MM.DD HH24:MI.SS');
/*
COUNT(ORDERID)
--------------
             2

Plan hash value: 4205400077

----------------------------------------------------------------------------------
| Id  | Operation          | Name        | Rows  | Bytes | Cost (%CPU)| Time     |
----------------------------------------------------------------------------------
|   0 | SELECT STATEMENT   |             |     1 |    21 |    69   (2)| 00:00:01 |
|   1 |  SORT AGGREGATE    |             |     1 |    21 |            |          |
|*  2 |   TABLE ACCESS FULL| GUEST_ORDER | 26477 |   542K|    69   (2)| 00:00:01 |
----------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

   2 - filter("CLOSE_DATE"<=TO_DATE(' 2015-12-31 15:30:00', 'syyyy-mm-dd
              hh24:mi:ss') AND "CLOSUREID" IS NULL)



--overit uzavreni vsech objednavek
*/
SELECT COUNT(orderid) FROM guest_order WHERE close_date is NULL;
/*
COUNT(ORDERID)
--------------
             0

Plan hash value: 4205400077

----------------------------------------------------------------------------------
| Id  | Operation          | Name        | Rows  | Bytes | Cost (%CPU)| Time     |
----------------------------------------------------------------------------------
|   0 | SELECT STATEMENT   |             |     1 |     8 |    68   (0)| 00:00:01 |
|   1 |  SORT AGGREGATE    |             |     1 |     8 |            |          |
|*  2 |   TABLE ACCESS FULL| GUEST_ORDER |   500 |  4000 |    68   (0)| 00:00:01 |
----------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

   2 - filter("CLOSE_DATE" IS NULL)



--vybrat objednavky, ktere budou upridany k uzaverce
*/
SELECT * FROM guest_order WHERE closureid is NULL and close_date <= TO_DATE('2015.12.31 15:30.00','YYYY.MM.DD HH24:MI.SS');
/*
OPEN_DATE CLOSE_DATE    ORDERID EMPLOYEEID GUEST_TABLEID  CLOSUREID
--------- ---------- ---------- ---------- ------------- ----------
30.12.15  31.12.15        30709        604          1668
30.12.15  31.12.15        30710        604          1668

Plan hash value: 502583569

---------------------------------------------------------------------------------
| Id  | Operation         | Name        | Rows  | Bytes | Cost (%CPU)| Time     |
---------------------------------------------------------------------------------
|   0 | SELECT STATEMENT  |             | 26477 |   749K|    69   (2)| 00:00:01 |
|*  1 |  TABLE ACCESS FULL| GUEST_ORDER | 26477 |   749K|    69   (2)| 00:00:01 |
---------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

   1 - filter("CLOSE_DATE"<=TO_DATE(' 2015-12-31 15:30:00', 'syyyy-mm-dd
              hh24:mi:ss') AND "CLOSUREID" IS NULL)

--spustit vytvoreni
*/
BEGIN
   rservice.createClosure(TO_DATE('2015.12.31 15:30.00','YYYY.MM.DD HH24:MI.SS'));
END;
/*
anonymous block completed


--overit vytvoreni nove uzaverky
*/
SELECT * FROM closure where closure_date = TO_DATE('2015.12.31 15:30.00','YYYY.MM.DD HH24:MI.SS');
/*
CLOSURE_DATE  CLOSUREID
------------ ----------
31.12.15           1811

Plan hash value: 2050925299

-----------------------------------------------------------------------------
| Id  | Operation         | Name    | Rows  | Bytes | Cost (%CPU)| Time     |
-----------------------------------------------------------------------------
|   0 | SELECT STATEMENT  |         |     1 |    12 |     3   (0)| 00:00:01 |
|*  1 |  TABLE ACCESS FULL| CLOSURE |     1 |    12 |     3   (0)| 00:00:01 |
-----------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

   1 - filter("CLOSURE_DATE"=TO_DATE(' 2015-12-31 15:30:00',
              'syyyy-mm-dd hh24:mi:ss'))



--overit prirazeni uzaverky k objednavce
*/
SELECT * FROM guest_order WHERE closureid = 1811;
/*
OPEN_DATE CLOSE_DATE    ORDERID EMPLOYEEID GUEST_TABLEID  CLOSUREID
--------- ---------- ---------- ---------- ------------- ----------
30.12.15  31.12.15        30709        604          1668       1811
30.12.15  31.12.15        30710        604          1668       1811

Plan hash value: 502583569

---------------------------------------------------------------------------------
| Id  | Operation         | Name        | Rows  | Bytes | Cost (%CPU)| Time     |
---------------------------------------------------------------------------------
|   0 | SELECT STATEMENT  |             |     1 |    29 |    69   (2)| 00:00:01 |
|*  1 |  TABLE ACCESS FULL| GUEST_ORDER |     1 |    29 |    69   (2)| 00:00:01 |
---------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

   1 - filter("CLOSUREID"=1811)
*/