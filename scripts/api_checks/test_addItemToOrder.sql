/*--vypsat polozky na objednavce
SET AUTOTREACE ON;*/
SELECT * FROM order_item where orderid = 29727;
/*
line 2: SQLPLUS Command Skipped: set AUTOTREACE ON
     COUNT ORDER_ITEMID     ITEMID    ORDERID
---------- ------------ ---------- ----------
        14       285168       8803      29727
         3       285169       8804      29727
         2       275300       7405      29727
         1       275301       9474      29727
         3       275302       6129      29727
         0       275303       8321      29727
         3       275304       7834      29727
         5       275305       9576      29727
         2       275306       8258      29727
         4       275307       6879      29727
         1       275308       7973      29727
         3       275309      10173      29727
         0       275310       7680      29727
         1       275311       7767      29727
         5       275312       8370      29727
         4       275313       8037      29727
         2       275314      10391      29727
         2       275315       7087      29727
         2       275298       6320      29727
         5       275299       6163      29727

 20 rows selected

Plan hash value: 753854098

--------------------------------------------------------------------------------
| Id  | Operation         | Name       | Rows  | Bytes | Cost (%CPU)| Time     |
--------------------------------------------------------------------------------
|   0 | SELECT STATEMENT  |            |     9 |   153 |   242   (2)| 00:00:03 |
|*  1 |  TABLE ACCESS FULL| ORDER_ITEM |     9 |   153 |   242   (2)| 00:00:03 |
--------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

   1 - filter("ORDERID"=29727)
*/

BEGIN
  rservice.additemtoorder(10516, 29727, 2);
END;
/*
anonymous block completed


--overit pridani polozky
*/
SELECT * FROM order_item where orderid = 29727 AND itemid = 10516;

/*
ine 12: SQLPLUS Command Skipped: set AUTOTREACE ON
     COUNT ORDER_ITEMID     ITEMID    ORDERID
---------- ------------ ---------- ----------
         2       285170      10516      29727

Plan hash value: 753854098

--------------------------------------------------------------------------------
| Id  | Operation         | Name       | Rows  | Bytes | Cost (%CPU)| Time     |
--------------------------------------------------------------------------------
|   0 | SELECT STATEMENT  |            |     1 |    17 |   242   (2)| 00:00:03 |
|*  1 |  TABLE ACCESS FULL| ORDER_ITEM |     1 |    17 |   242   (2)| 00:00:03 |
--------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

   1 - filter("ORDERID"=29727 AND "ITEMID"=10516)



--pridat polozku ktera existuje
*/
BEGIN
  rservice.additemtoorder(10516, 29727, 2);
END;
/*
anonymous block completed


--overit pridani polozky
*/

SELECT * FROM order_item where orderid = 29727 AND itemid = 10516;
/*
line 21: SQLPLUS Command Skipped: set AUTOTREACE ON
     COUNT ORDER_ITEMID     ITEMID    ORDERID
---------- ------------ ---------- ----------
         4       285170      10516      29727

Plan hash value: 753854098

--------------------------------------------------------------------------------
| Id  | Operation         | Name       | Rows  | Bytes | Cost (%CPU)| Time     |
--------------------------------------------------------------------------------
|   0 | SELECT STATEMENT  |            |     1 |    17 |   242   (2)| 00:00:03 |
|*  1 |  TABLE ACCESS FULL| ORDER_ITEM |     1 |    17 |   242   (2)| 00:00:03 |
--------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

   1 - filter("ORDERID"=29727 AND "ITEMID"=10516)
*/
