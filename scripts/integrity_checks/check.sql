Autotrace Enabled
SET AUTOTRACE ON;
/*Shows the execution plan as well as statistics of the statement.

--Polozka smi bzt na objednavce jen jednou, pocet polozek je dan v asociacni tabulce (ORDER_ITEM.COUNT).

Error starting at line 3 in command:*/
INSERT INTO ORDER_ITEM VALUES (1,NULL, 7405, 29727)/*
Error report:
SQL Error: ORA-20000: This item is already on order!
ORA-06512: na "DOKOMESPA1.CHECK_ORDER_ITEM_UNIQ", line 9
ORA-06512: na "DOKOMESPA1.CHECK_NEW_ORDER_ITEM", line 2
ORA-04088: chyba během provádění triggeru 'DOKOMESPA1.CHECK_NEW_ORDER_ITEM'
20000. 00000 -  "%s"
*Cause:    The stored procedure 'raise_application_error'
was called which causes this error to be generated.
*Action:   Correct the problem as described in the error message or contact
the application administrator or DBA for more information.

---------------------------------------------------------------------------------------
| Id  | Operation                | Name       | Rows  | Bytes | Cost (%CPU)| Time     |
---------------------------------------------------------------------------------------
|   0 | INSERT STATEMENT         |            |     1 |    17 |     1   (0)| 00:00:01 |
|   1 |  LOAD TABLE CONVENTIONAL | ORDER_ITEM |       |       |            |          |
---------------------------------------------------------------------------------------


--Pocet polozek v asociacni tabulce ORDER_ITEM musi byt >= 1.
Error starting at line 5 in command:*/
INSERT INTO order_item values(0,NULL,0,0)/*
Error report:
SQL Error: ORA-20000: Items count must be >= 1!
ORA-06512: na "DOKOMESPA1.CHECK_ORDER_ITEM_COUNT", line 7
ORA-06512: na "DOKOMESPA1.ORDER_ITEM_COUNT", line 2
ORA-04088: chyba během provádění triggeru 'DOKOMESPA1.ORDER_ITEM_COUNT'
20000. 00000 -  "%s"
*Cause:    The stored procedure 'raise_application_error'
was called which causes this error to be generated.
*Action:   Correct the problem as described in the error message or contact
the application administrator or DBA for more information.

---------------------------------------------------------------------------------------
| Id  | Operation                | Name       | Rows  | Bytes | Cost (%CPU)| Time     |
---------------------------------------------------------------------------------------
|   0 | INSERT STATEMENT         |            |     1 |    17 |     1   (0)| 00:00:01 |
|   1 |  LOAD TABLE CONVENTIONAL | ORDER_ITEM |       |       |            |          |
---------------------------------------------------------------------------------------


--Uzaverka prirazena objednavce musi byt nejblizsi uzaverkou s datem vyssim, nez datum uzavreni objednavky.
Error starting at line 8 in command:*/
INSERT INTO guest_order values(TO_DATE('2014.12.30 12:10.00','YYYY.MM.DD HH24:MI.SS'), TO_DATE('2014.12.30 12:11.00','YYYY.MM.DD HH24:MI.SS'), NULL, 604, 1668, 1806)/*
Error report:
SQL Error: ORA-20000: Close_date must be higher then closure date and higher then last closure date!
ORA-06512: na "DOKOMESPA1.CHECK_ORDER_CLOSURE_UPDATE", line 17
ORA-06512: na "DOKOMESPA1.CHECK_ORDER_CLOSURE_UPDATE", line 2
ORA-04088: chyba během provádění triggeru 'DOKOMESPA1.CHECK_ORDER_CLOSURE_UPDATE'
20000. 00000 -  "%s"
*Cause:    The stored procedure 'raise_application_error'
was called which causes this error to be generated.
*Action:   Correct the problem as described in the error message or contact
the application administrator or DBA for more information.

----------------------------------------------------------------------------------------
| Id  | Operation                | Name        | Rows  | Bytes | Cost (%CPU)| Time     |
----------------------------------------------------------------------------------------
|   0 | INSERT STATEMENT         |             |     1 |    29 |     1   (0)| 00:00:01 |
|   1 |  LOAD TABLE CONVENTIONAL | GUEST_ORDER |       |       |            |          |
----------------------------------------------------------------------------------------


--Datum vytvoreni objednavky musi byt mensi, nez datum uzavreni objednavky.
--closure 25.12.15	1806

Error starting at line 11 in command:*/
INSERT INTO guest_order values(TO_DATE('2015.12.24 12:10.00','YYYY.MM.DD HH24:MI.SS'), TO_DATE('2015.12.24 12:09.00','YYYY.MM.DD HH24:MI.SS'), NULL, 604, 1668, 1806)/*
Error report:
SQL Error: ORA-20000: Close date is smaller then open date!
ORA-06512: na "DOKOMESPA1.CHECK_ORDER_CLOSURE_DATE", line 8
ORA-06512: na "DOKOMESPA1.CREATION_SMALLER_CLOSE", line 2
ORA-04088: chyba během provádění triggeru 'DOKOMESPA1.CREATION_SMALLER_CLOSE'
20000. 00000 -  "%s"
*Cause:    The stored procedure 'raise_application_error'
was called which causes this error to be generated.
*Action:   Correct the problem as described in the error message or contact
the application administrator or DBA for more information.

----------------------------------------------------------------------------------------
| Id  | Operation                | Name        | Rows  | Bytes | Cost (%CPU)| Time     |
----------------------------------------------------------------------------------------
|   0 | INSERT STATEMENT         |             |     1 |    29 |     1   (0)| 00:00:01 |
|   1 |  LOAD TABLE CONVENTIONAL | GUEST_ORDER |       |       |            |          |
----------------------------------------------------------------------------------------


--Na kazdem stolu muze byt pouze jedna aktivni objednavka.

Error starting at line 13 in command:*/
INSERT INTO CLOSURE values(TO_DATE('2015.05.01 12:10.00','YYYY.MM.DD HH24:MI.SS'),NULL)/*
Error report:
SQL Error: ORA-20000: There is an active order!
ORA-06512: na "DOKOMESPA1.CHECK_OPEN_ORDERS", line 9
ORA-06512: na "DOKOMESPA1.CHECK_OPEN_ORDERS", line 2
ORA-04088: chyba během provádění triggeru 'DOKOMESPA1.CHECK_OPEN_ORDERS'
20000. 00000 -  "%s"
*Cause:    The stored procedure 'raise_application_error'
was called which causes this error to be generated.
*Action:   Correct the problem as described in the error message or contact
the application administrator or DBA for more information.

------------------------------------------------------------------------------------
| Id  | Operation                | Name    | Rows  | Bytes | Cost (%CPU)| Time     |
------------------------------------------------------------------------------------
|   0 | INSERT STATEMENT         |         |     1 |    12 |     1   (0)| 00:00:01 |
|   1 |  LOAD TABLE CONVENTIONAL | CLOSURE |       |       |            |          |
------------------------------------------------------------------------------------


--Na každém stole může být pouze jedna aktivní objednávka.

Error starting at line 15 in command:*/
INSERT INTO GUEST_ORDER values(TO_DATE('2015.05.01 12:10.00','YYYY.MM.DD HH24:MI.SS'),NULL, NULL,604,1668,NULL)/*
Error report:
SQL Error: ORA-20000: There is an active order on this table already!
ORA-06512: na "DOKOMESPA1.CHECK_NEW_ACTIVE_ORDER", line 14
ORA-06512: na "DOKOMESPA1.CHECK_NEW_ACTIVE_ORDER", line 2
ORA-04088: chyba během provádění triggeru 'DOKOMESPA1.CHECK_NEW_ACTIVE_ORDER'
20000. 00000 -  "%s"
*Cause:    The stored procedure 'raise_application_error'
was called which causes this error to be generated.
*Action:   Correct the problem as described in the error message or contact
the application administrator or DBA for more information.

----------------------------------------------------------------------------------------
| Id  | Operation                | Name        | Rows  | Bytes | Cost (%CPU)| Time     |
----------------------------------------------------------------------------------------
|   0 | INSERT STATEMENT         |             |     1 |    29 |     1   (0)| 00:00:01 |
|   1 |  LOAD TABLE CONVENTIONAL | GUEST_ORDER |       |       |            |          |
----------------------------------------------------------------------------------------


--Datum nove vkladane uzaverky musi byt vetsí, nez nejvyssi datum v tabulce CLOSURE.

Error starting at line 17 in command:*/
INSERT INTO CLOSURE values(TO_DATE('2015.08.01 12:10.00','YYYY.MM.DD HH24:MI.SS'),NULL)/*
Error report:
SQL Error: ORA-20000: There is an active order!
ORA-06512: na "DOKOMESPA1.CHECK_OPEN_ORDERS", line 9
ORA-06512: na "DOKOMESPA1.CHECK_OPEN_ORDERS", line 2
ORA-04088: chyba během provádění triggeru 'DOKOMESPA1.CHECK_OPEN_ORDERS'
20000. 00000 -  "%s"
*Cause:    The stored procedure 'raise_application_error'
was called which causes this error to be generated.
*Action:   Correct the problem as described in the error message or contact
the application administrator or DBA for more information.

------------------------------------------------------------------------------------
| Id  | Operation                | Name    | Rows  | Bytes | Cost (%CPU)| Time     |
------------------------------------------------------------------------------------
|   0 | INSERT STATEMENT         |         |     1 |    12 |     1   (0)| 00:00:01 |
|   1 |  LOAD TABLE CONVENTIONAL | CLOSURE |       |       |            |          |
------------------------------------------------------------------------------------


--Hodnoty u pole ACTIVE v tabulce ITEM jsou booleanovske a smeji tedy nabyvat pouze hodnot 0 a 1.

Error starting at line 19 in command:*/
INSERT INTO ITEM values('I1',10,20,9,NULL,1806)/*
Error report:
SQL Error: ORA-20000: Atempt to insert ilegal value!
ORA-06512: na "DOKOMESPA1.CHECK_IS_BOOLEAN", line 7
ORA-06512: na "DOKOMESPA1.CHECK_ACTIVE_BOOL_VAL", line 2
ORA-04088: chyba během provádění triggeru 'DOKOMESPA1.CHECK_ACTIVE_BOOL_VAL'
20000. 00000 -  "%s"
*Cause:    The stored procedure 'raise_application_error'
was called which causes this error to be generated.
*Action:   Correct the problem as described in the error message or contact
the application administrator or DBA for more information.

---------------------------------------------------------------------------------
| Id  | Operation                | Name | Rows  | Bytes | Cost (%CPU)| Time     |
---------------------------------------------------------------------------------
|   0 | INSERT STATEMENT         |      |     1 |    28 |     1   (0)| 00:00:01 |
|   1 |  LOAD TABLE CONVENTIONAL | ITEM |       |       |            |          |
---------------------------------------------------------------------------------

