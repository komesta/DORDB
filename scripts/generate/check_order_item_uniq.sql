CREATE OR REPLACE PROCEDURE CHECK_ORDER_ITEM_UNIQ
(new_itemID IN NUMBER, new_orderID IN nUMBER)
IS
cnt  NUMBER;
BEGIN
  SELECT COUNT(ORDER_ITEMID) into cnt FROM ORDER_ITEM where ORDERID = new_orderID AND ITEMID = new_itemID;
  IF cnt != 0 then
    DBMS_OUTPUT.PUT_LINE('This item is already on order!');
    raise_application_error(-20000, 'This item is already on order!');
  END IF;
END;
/
CREATE OR REPLACE TRIGGER CHECK_NEW_ORDER_ITEM
BEFORE INSERT
ON ORDER_ITEM
FOR EACH ROW
BEGIN
  CHECK_ORDER_ITEM_UNIQ(:new.ITEMID, :new.ORDERID);
END;
/