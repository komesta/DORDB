CREATE OR REPLACE PROCEDURE CHECK_NEW_ACTIVE_ORDER
(tableId IN NUMBER, close_date DATE)
IS
activeOrdersOnTable NUMBER;
BEGIN
--pokud to neni nova aktivni objednavka, tak nic
  IF close_date is not null THEN
    RETURN;
  END IF;
  select COUNT(GUEST_ORDER.ORDERID) into activeOrdersOnTable from GUEST_ORDER where GUEST_ORDER.GUEST_TABLEID = tableId and GUEST_ORDER.CLOSE_DATE is null;
  IF activeOrdersOnTable > 0 then
    DBMS_OUTPUT.PUT_LINE('There is an active order on this table already!');
    raise_application_error(-20000, 'There is an active order on this table already!');
  END IF;
END;
/
CREATE OR REPLACE TRIGGER CHECK_NEW_ACTIVE_ORDER
BEFORE INSERT
ON GUEST_ORDER
FOR EACH ROW
BEGIN
  CHECK_NEW_ACTIVE_ORDER(:new.GUEST_TABLEID, :new.CLOSE_DATE);
END;
/