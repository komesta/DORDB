CREATE OR REPLACE PROCEDURE CHECK_ORDER_CLOSURE_UPDATE
(newClosureId IN NUMBER, newCloseDate IN DATE)
IS
cid NUMBER;
BEGIN
  IF newClosureId is null then
    RETURN;
  END IF;
  IF newCloseDate is null then
    DBMS_OUTPUT.PUT_LINE('Cannot save order with closure and without closeDate!');
    raise_application_error(-20000, 'Cannot save order with closure and without closeDate!');
  END IF;
  -- musi to byt nejblizsi vetsi uzaverka
  select closureid into cid from (select closureid from CLOSURE where closure_date > newCloseDate order by closure_date desc) where rownum = 1;
  IF cid != newClosureId then
    DBMS_OUTPUT.PUT_LINE('Close_date must be higher then closure date and higher then last closure date!');
    raise_application_error(-20000, 'Close_date must be higher then closure date and higher then last closure date!');
  END IF;
END;
/
create or replace
trigger CHECK_ORDER_CLOSURE_UPDATE
BEFORE INSERT OR UPDATE
ON GUEST_ORDER
FOR EACH ROW
BEGIN
  CHECK_ORDER_CLOSURE_UPDATE(:new.CLOSUREID,:new.CLOSE_DATE);
END;
/