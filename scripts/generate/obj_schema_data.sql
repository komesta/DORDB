SET AUTOTRACE ON;
insert into OBJ_CATEGORY select name, category_id from CATEGORY;
/
insert into OBJ_ITEM select name, price_buy, price_sell, active, itemid, NULL  from ITEM;
/
update OBJ_ITEM obj_it
  set obj_it.category_ref =
    (SELECT REF(obj_category)
        FROM OBJ_CATEGORY obj_category
        JOIN CATEGORY c ON obj_category.category_id = c.category_id
        JOIN ITEM i ON i.categoryid = c.category_id
        WHERE i.ITEMID = obj_it.ITEMID
        );
/
insert into OBJ_CLOSURE select closure_date, closureid  from CLOSURE;
/
insert into OBJ_EMPLOYEE select surname, name, employeeid  from EMPLOYEE;
/
insert into OBJ_GUEST_TABLE select table_number, guest_tableid  from guest_table;
/
insert into OBJ_GUEST_ORDER select open_date, close_date, orderid, null, null, null from guest_order;
/
update OBJ_GUEST_ORDER obj_go
  set obj_go.employee_ref=
    (SELECT REF(obj_employee)
        FROM OBJ_EMPLOYEE obj_employee
        JOIN EMPLOYEE e ON obj_employee.employeeid = e.employeeid
        JOIN GUEST_ORDER o ON o.employeeid = e.employeeid
        WHERE o.orderid = obj_go.orderid
        );
/
update OBJ_GUEST_ORDER obj_go
  set obj_go.guest_table_ref=
    (SELECT REF(obj_guest_table)
        FROM obj_guest_table obj_guest_table
        JOIN GUEST_TABLE e ON obj_guest_table.guest_tableid = e.guest_tableid
        JOIN GUEST_ORDER o ON o.guest_tableid = e.guest_tableid
        WHERE o.orderid = obj_go.orderid
        );
/
update OBJ_GUEST_ORDER obj_go
  set obj_go.closure_ref=
    (SELECT REF(obj_closure)
        FROM obj_closure obj_closure
        JOIN CLOSURE e ON obj_closure.closure_id = e.closureid
        JOIN GUEST_ORDER o ON o.closureid = e.closureid
        WHERE o.orderid = obj_go.orderid
        );
/
insert into OBJ_ORDER_ITEM select count, order_itemid, null, null from order_item;
/
update OBJ_ORDER_ITEM obj_oi
  set obj_oi.item_ref=
    (SELECT REF(obj_item)
        FROM obj_item obj_item
        JOIN ITEM e ON obj_item.itemid = e.itemid
        JOIN ORDER_ITEM o ON o.itemid = e.itemid
        WHERE o.order_itemid = obj_oi.order_itemid
        );
/
update OBJ_ORDER_ITEM obj_oi
  set obj_oi.order_ref=
    (SELECT REF(obj_guest_order)
        FROM obj_guest_order obj_guest_order
        JOIN GUEST_ORDER e ON obj_guest_order.orderid = e.orderid
        JOIN ORDER_ITEM o ON o.orderid = obj_guest_order.orderid
        WHERE o.order_itemid = obj_oi.order_itemid
        );