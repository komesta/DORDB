CREATE OR REPLACE PROCEDURE CHECK_OPEN_ORDERS
IS
activeOrders NUMBER;
BEGIN 
  select COUNT(GUEST_ORDER.ORDERID) into activeOrders from GUEST_ORDER where GUEST_ORDER.CLOSE_DATE is null;
   DBMS_OUTPUT.PUT_LINE(activeOrders);
  IF activeOrders > 0 then
    DBMS_OUTPUT.PUT_LINE('There is an active order!');
    raise_application_error(-20000, 'There is an active order!');
  END IF;
END;
/
CREATE OR REPLACE TRIGGER CHECK_OPEN_ORDERS
BEFORE INSERT OR UPDATE
ON CLOSURE
FOR EACH ROW
BEGIN
  CHECK_OPEN_ORDERS();
END;
/