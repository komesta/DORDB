CREATE OR REPLACE PACKAGE RSERVICE AS
PROCEDURE addItemToOrder(item_id IN NUMBER, order_id IN NUMBER, cnt IN NUMBER);
PROCEDURE createClosure(datum IN DATE);
END RSERVICE;
/
CREATE OR REPLACE PACKAGE BODY RSERVICE AS
PROCEDURE addItemToOrder(item_id IN NUMBER, order_id IN NUMBER,  cnt IN NUMBER) IS
  exist NUMBER;
  itemsCount NUMBER;
   BEGIN
     SELECT COUNT(order_itemid) INTO exist FROM order_item WHERE itemid = item_id AND orderid = order_id;
     IF exist = 0 THEN
      INSERT INTO order_item VALUES(cnt, NULL, item_id, order_id);
      RETURN;
     END IF;
     IF exist = 1 THEN
      SELECT order_item.count INTO itemsCount FROM order_item WHERE itemid = item_id AND orderid = order_id;
      UPDATE order_item SET count = itemsCount + cnt WHERE itemid = item_id AND orderid = order_id;
      RETURN;
     END IF;
      raise_application_error(-20000, 'Item is multiple times on order!');
   END;


  PROCEDURE createClosure(datum IN DATE) IS
  newOrdersCount NUMBER;
  unclosedOrdersCount NUMBER;
  v_id NUMBER;
  BEGIN
    SELECT COUNT(orderid) INTO newOrdersCount FROM guest_order WHERE closureid is NULL and close_date <= datum;
    IF newOrdersCount = 0 THEN
     raise_application_error(-20000, 'No new orders!');
    ELSE
      --existuji nove objednavky
      SELECT COUNT(orderid) INTO unclosedOrdersCount FROM guest_order WHERE close_date is NULL;
      IF unclosedOrdersCount != 0 THEN
        raise_application_error(-20000, 'Unclosed orders exists!');
      ELSE
        --vse je v poradku
        INSERT INTO closure VALUES(datum, NULL) returning closureid into v_id;
        UPDATE guest_order SET closureid = v_id WHERE closureid is NULL and close_date <= datum;
      END IF;
    END IF;
  END;
END RSERVICE;
/
