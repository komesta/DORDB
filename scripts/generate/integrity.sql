CREATE OR REPLACE PROCEDURE CHECK_NEW_ACTIVE_ORDER
(tableId IN NUMBER, close_date DATE)
IS
activeOrdersOnTable NUMBER;
BEGIN
--pokud to neni nova aktivni objednavka, tak nic
  IF close_date is not null THEN
    RETURN;
  END IF;
  select COUNT(GUEST_ORDER.ORDERID) into activeOrdersOnTable from GUEST_ORDER where GUEST_ORDER.GUEST_TABLEID = tableId and GUEST_ORDER.CLOSE_DATE is null;
  IF activeOrdersOnTable > 0 then
    DBMS_OUTPUT.PUT_LINE('There is an active order on this table already!');
    raise_application_error(-20000, 'There is an active order on this table already!');
  END IF;
END;
/
CREATE OR REPLACE TRIGGER CHECK_NEW_ACTIVE_ORDER
BEFORE INSERT
ON GUEST_ORDER
FOR EACH ROW
BEGIN
  CHECK_NEW_ACTIVE_ORDER(:new.GUEST_TABLEID, :new.CLOSE_DATE);
END;
/

CREATE OR REPLACE PROCEDURE CHECK_CLOSURE_DATE
(newCloserDate IN DATE)
IS
maxDate DATE;
BEGIN
  select MAX(CLOSURE.CLOSURE_DATE) into maxDate from CLOSURE;
  DBMS_OUTPUT.PUT_LINE(maxDate);
  IF maxDate >= newCloserDate then
    DBMS_OUTPUT.PUT_LINE('New closure date must be maximal date in closures!');
    raise_application_error(-20000, 'New closure date must be maximal date in closures!');
  END IF;
END;
/
CREATE OR REPLACE TRIGGER CHECK_CLOSURE_DATE
BEFORE INSERT OR UPDATE
ON CLOSURE
FOR EACH ROW
BEGIN
  CHECK_CLOSURE_DATE(:new.CLOSURE_DATE);
END;
/

CREATE OR REPLACE PROCEDURE CHECK_IS_BOOLEAN
(val IN NUMBER)
IS
BEGIN
  IF val != 0 and val != 1 THEN
    DBMS_OUTPUT.PUT_LINE('Atempt to insert ilegal value!!');
    raise_application_error(-20000, 'Atempt to insert ilegal value!');
  END IF;
END;
/
CREATE OR REPLACE TRIGGER CHECK_ACTIVE_BOOL_VAL
BEFORE INSERT OR UPDATE
ON ITEM
FOR EACH ROW
BEGIN
  CHECK_IS_BOOLEAN(:new.ACTIVE);
END;
/

CREATE OR REPLACE PROCEDURE CHECK_OPEN_ORDERS
IS
activeOrders NUMBER;
BEGIN
  select COUNT(GUEST_ORDER.ORDERID) into activeOrders from GUEST_ORDER where GUEST_ORDER.CLOSE_DATE is null;
   DBMS_OUTPUT.PUT_LINE(activeOrders);
  IF activeOrders > 0 then
    DBMS_OUTPUT.PUT_LINE('There is an active order!');
    raise_application_error(-20000, 'There is an active order!');
  END IF;
END;
/
CREATE OR REPLACE TRIGGER CHECK_OPEN_ORDERS
BEFORE INSERT OR UPDATE
ON CLOSURE
FOR EACH ROW
BEGIN
  CHECK_OPEN_ORDERS();
END;
/

CREATE OR REPLACE PROCEDURE CHECK_ORDER_CLOSURE_UPDATE
(newClosureId IN NUMBER, newCloseDate IN DATE)
IS
cid NUMBER;
BEGIN
  IF newClosureId is null then
    RETURN;
  END IF;
  IF newCloseDate is null then
    DBMS_OUTPUT.PUT_LINE('Cannot save order with closure and without closeDate!');
    raise_application_error(-20000, 'Cannot save order with closure and without closeDate!');
  END IF;
  -- musi to byt nejblizsi vetsi uzaverka
  select closureid into cid from (select closureid from CLOSURE where closure_date > newCloseDate order by closure_date desc) where rownum = 1;
  IF cid != newClosureId then
    DBMS_OUTPUT.PUT_LINE('Close_date must be higher then closure date and higher then last closure date!');
    raise_application_error(-20000, 'Close_date must be higher then closure date and higher then last closure date!');
  END IF;
END;
/
create or replace
trigger CHECK_ORDER_CLOSURE_UPDATE
BEFORE INSERT OR UPDATE
ON GUEST_ORDER
FOR EACH ROW
BEGIN
  CHECK_ORDER_CLOSURE_UPDATE(:new.CLOSUREID,:new.CLOSE_DATE);
END;
/

CREATE OR REPLACE PROCEDURE CHECK_ORDER_CLOSURE_DATE
(openDate IN DATE, closeDate IN DATE)
IS
BEGIN
  IF closeDate is not NULL then
    IF closeDate <= openDate then
      DBMS_OUTPUT.PUT_LINE('Close date is smaller then open date!');
      raise_application_error(-20000, 'Close date is smaller then open date!');
    END IF;
  END IF;

END;
/
CREATE OR REPLACE TRIGGER CREATION_SMALLER_CLOSE
BEFORE INSERT OR UPDATE
ON GUEST_ORDER
FOR EACH ROW
BEGIN
  CHECK_ORDER_CLOSURE_DATE(:new.OPEN_DATE, :new.CLOSE_DATE);
END;
/

CREATE OR REPLACE PROCEDURE CHECK_ORDER_ITEM_COUNT
(itemCount IN NUMBER)
IS
BEGIN
  IF itemCount < 1 then
    DBMS_OUTPUT.PUT_LINE('Items count must be >= 1!');
    raise_application_error(-20000, 'Items count must be >= 1!');
  END IF;

END;
/
CREATE OR REPLACE TRIGGER ORDER_ITEM_COUNT
BEFORE INSERT OR UPDATE
ON ORDER_ITEM
FOR EACH ROW
BEGIN
  CHECK_ORDER_ITEM_COUNT(:new.COUNT);
END;
/
CREATE OR REPLACE PROCEDURE CHECK_ORDER_ITEM_UNIQ
(new_itemID IN NUMBER, new_orderID IN nUMBER)
IS
cnt  NUMBER;
BEGIN
  SELECT COUNT(ORDER_ITEMID) into cnt FROM ORDER_ITEM where ORDERID = new_orderID AND ITEMID = new_itemID;
  IF cnt != 0 then
    DBMS_OUTPUT.PUT_LINE('This item is already on order!');
    raise_application_error(-20000, 'This item is already on order!');
  END IF;
END;
/
CREATE OR REPLACE TRIGGER CHECK_NEW_ORDER_ITEM
BEFORE INSERT
ON ORDER_ITEM
FOR EACH ROW
BEGIN
  CHECK_ORDER_ITEM_UNIQ(:new.ITEMID, :new.ORDERID);
END;
/