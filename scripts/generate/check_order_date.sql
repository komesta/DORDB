CREATE OR REPLACE PROCEDURE CHECK_ORDER_CLOSURE_DATE
(openDate IN DATE, closeDate IN DATE)
IS
BEGIN
  IF closeDate is not NULL then
    IF closeDate <= openDate then
      DBMS_OUTPUT.PUT_LINE('Close date is smaller then open date!');
      raise_application_error(-20000, 'Close date is smaller then open date!');
    END IF;
  END IF;

END;
/
CREATE OR REPLACE TRIGGER CREATION_SMALLER_CLOSE
BEFORE INSERT OR UPDATE
ON GUEST_ORDER
FOR EACH ROW
BEGIN
  CHECK_ORDER_CLOSURE_DATE(:new.OPEN_DATE, :new.CLOSE_DATE);
END;
/