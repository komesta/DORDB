CREATE OR REPLACE PROCEDURE CHECK_CLOSURE_DATE
(newCloserDate IN DATE)
IS
maxDate DATE;
BEGIN

  select MAX(CLOSURE.CLOSURE_DATE) into maxDate from CLOSURE;
  DBMS_OUTPUT.PUT_LINE(maxDate);
  IF maxDate >= newCloserDate then
    DBMS_OUTPUT.PUT_LINE('New closure date must be maximal date in closures!');
    raise_application_error(-20000, 'New closure date must be maximal date in closures!');
  END IF;
END;
/
CREATE OR REPLACE TRIGGER CHECK_CLOSURE_DATE
BEFORE INSERT OR UPDATE
ON CLOSURE
FOR EACH ROW
BEGIN
  CHECK_CLOSURE_DATE(:new.CLOSURE_DATE);
END;
/