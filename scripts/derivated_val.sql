ALTER TABLE ITEM ADD (profit NUMBER(19,4));
/
create or replace 
trigger DERIVE_PROFIT
BEFORE INSERT OR UPDATE
ON ITEM
FOR EACH ROW
BEGIN
  :new.profit := :new.price_sell - :new.price_buy;
END;
/
INSERT INTO ITEM VALUES('t1',10,40,1,NULL,9180,NULL);